from textgenrnn import textgenrnn
import os
import pyarabic.araby as araby
from IPython import embed


directory=os.path.join('.','ArabicText')
files=os.listdir(directory)
data=[]
for file in files:
    with open(os.path.join(directory,file),'r',encoding='utf-16') as f:
        thisfile=f.readlines()
        thisfile=[d.strip() for d in thisfile]
        thisfile=" ".join(thisfile)
        data.append(thisfile)
        
textgen=textgenrnn()

textgen.train_on_texts(data,num_epochs=200,gen_epochs=200)
textgen.save(weights_path="arabicText.hdf5")
